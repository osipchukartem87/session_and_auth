@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Title</th>
                    <th scope="col">Body</th>
                    <th scope="col">Actions</th>
                    <th scope="col">Status</th>
                </tr>
                </thead>
                <tbody>
                @foreach($articles as $article)
                    <tr>
                        <td scope="row">
                            {{ $article->id }}
                        </td>
                        <td>
                            {{ $article->title }}
                        </td>
                        <td>
                            {{ strlen($article->body) > 50 ? substr($in,0,50)."..." : $article->body }}
                        </td>
                        <td>
                            <a href="{{ route('article.show', $article) }}">Show</a> |
                            <a href="{{ route('article.edit', $article) }}">Edit</a> |
                            <a href="{{ route('article.moderate', $article) }}"
                               onclick="event.preventDefault(); document.getElementById('submit-form').submit()">
                                Send to moderate
                            </a>

                            <form id="submit-form" action="{{ route('article.moderate', $article) }}" method="POST"
                                  class="hidden">
                                @csrf

                                @method('PUT')
                            </form>
                        </td>
                        <td>
                            {{ $article->status }}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
