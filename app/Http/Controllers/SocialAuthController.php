<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class SocialAuthController extends Controller
{
    public function redirectToProvider(Request $request)
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleProviderCallback()
    {

        $socialUser = Socialite::driver('google')->user();

        $user = User::where(['email' => $socialUser->email])->first();

        if (is_null($user)) {
            $user = User::create([
                'name' => $socialUser->user['login'],
                'email' => $socialUser->email,
            ]);
        }

        Auth::login($user);

        return redirect('/home');
    }
}
