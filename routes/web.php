<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\SocialAuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['middleware' => ['auth']], function ($router) {
    Route::resource('article', ArticleController::class, ['except' => ['show']]);
});

Route::get('/article/{article}', [ArticleController::class, 'show'])
    ->name('article.show')
    ->middleware('auth');
Route::put('/moderate/{article}', [ArticleController::class, 'toModerate'])->name('article.moderate');

Route::get('auth/google', [SocialAuthController::class, 'redirectToProvider']);
Route::get('auth/google/callback', [SocialAuthController::class, 'handleProviderCallback']);
